package ui;

import constants.Constants;
import domain.Friendship;
import domain.Message;
import domain.User;
import service.Service;
import service.dtos.UserFriendshipDTO;

import java.sql.SQLException;
import java.time.LocalDate;
import java.time.format.DateTimeParseException;
import java.util.List;
import java.util.Scanner;

public class UI {

    private final Scanner scanner;
    private final Service service;
    private boolean authenticated = false;


    public UI(Service service) {
        this.service = service;
        this.scanner = new Scanner(System.in);
    }

    public void start() {
        //noinspection InfiniteLoopStatement
        while (true) {
            loop();
        }
    }

    private void loop() {
        printMenu();
        String string = readLine(">>> ");

        try {
            if (authenticated) {
                switch (string) {
                    case "1" -> addFriend();
                    case "2" -> removeFriend();
                    case "3" -> listFriends();
                    case "4" -> signOut();
                    case "5" -> deleteAccount();
                    case "6" -> getCommunitiesNumber();
                    case "7" -> largestComunity();
                    case "8" -> updateUserName();
                    case "A" -> filterUserFriendships();
                    case "B" -> filterUserFriendshipsByMonth();
                    case "C" -> printOrderedMessagesForTwoUsers();
                    case "accept" -> acceptFriendRequest();
                    default -> {
                    }
                }
            } else {
                switch (string) {
                    case "1" -> signIn();
                    case "2" -> signUp();
                    case "3" -> System.exit(-1);
                    default -> {
                    }
                }
            }
        } catch (Error e) {
            System.out.println(e.getMessage());
        }
    }

    private void printMenu() {
        if (authenticated) {
            System.out.println("1. Add friend");
            System.out.println("2. Remove friend");
            System.out.println("3. Friends list");
            System.out.println("4. Sign out");
            System.out.println("5. Delete account");
            System.out.println("6. Communities number");
            System.out.println("7. Largest community");
            System.out.println("8. Update user name");
            System.out.println("A. Find a user's friendships");
            System.out.println("B. Find a user's friendships in a specific month");
            System.out.println("C. Get messages between 2 users in chronological order");
        } else {
            System.out.println("1. Sign in");
            System.out.println("2. Sign up");
            System.out.println("3. Exit");
        }
    }

    private String readLine(String helper) {
        System.out.print(helper);
        return scanner.nextLine();
    }

    private void signIn() {
        String email = readLine("Email: ");
        String password = readLine("Password: ");
        authenticated = service.loginUser(email, password);
        if (authenticated) {
            User user = service.getCurrentUser();
            System.out.println("Welcome " + user.getFirstName() + " " + user.getLastName());
        }
    }

    private void signUp() {
        String firstName = readLine("First Name: ");
        String lastName = readLine("Last Name: ");
        String email = readLine("Email: ");
        String password = readLine("Password: ");
        String birthdayString = readLine("BirthDay(dd-MM-yyyy): ");
        try {
            LocalDate birthday = LocalDate.parse(birthdayString, Constants.STANDARD_DATE_FORMAT);
            authenticated = service.createUser(firstName, lastName, email, password, birthday);
            if (!authenticated) {
                System.out.println("Invalid credentials");
            }
        } catch (DateTimeParseException e) {
            System.out.println(e.getMessage());
        }

    }

    private void signOut() {
        service.logoutUser();
        authenticated = false;
    }

    private void addFriend() {
        String friendId = readLine("FiendId: ");
        service.addFriend(friendId);
    }

    private void removeFriend() {
        String friendshipId = readLine("FriendshipId: ");
        service.removeFriend(friendshipId);
    }

    private void listFriends() {
        System.out.println("Friends: ");
        for (Friendship friendship : service.getUserFriendships()) {
            System.out.println(friendship);
        }
        System.out.println("---------");
    }

    private void deleteAccount() {
        service.deleteAccount();
        System.out.println("Account deleted");
        authenticated = false;
    }

    private void printOrderedMessagesForTwoUsers() {
        String UID1 = readLine("First user id: ");
        String UID2 = readLine("Second user id: ");
        try {
            List<Message> orderedMessaged = service.getOrderedMessagesForTwoUsers(UID1, UID2);
            orderedMessaged.forEach(System.out::println);
        } catch (SQLException e){
            System.out.println(e.getMessage());
        }
    }

    public void getCommunitiesNumber() {
        System.out.println("Communities: " + service.getCommunitiesNumber());
    }

    private void largestComunity(){
        System.out.println("Largest community: " + service.getLargestCommunity());
    }

    private void updateUserName() {
        String newFirstName = readLine("New First Name: ");
        String newLastName = readLine("New Last Name: ");
        service.updateUserName(newFirstName, newLastName);
    }

    private void filterUserFriendships() {
        String userID = readLine("User ID: ");
        for (UserFriendshipDTO dto : service.getUserFriendships(userID))
            System.out.println(dto);
    }

    private void filterUserFriendshipsByMonth() {
        String userID = readLine("User ID: ");
        try {
            int month = Integer.parseInt(readLine("Month (1-12)"));
            if (month < 1 || month > 12) {
                throw new Exception("Invalid month! (1-12)");
            }
            for (UserFriendshipDTO dto : service.getUserFriendshipsByMonth(userID, month))
                System.out.println(dto);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    private void acceptFriendRequest() {
        String userID = readLine("User ID: ");
        try {
            service.acceptFriendRequest(userID);
        } catch (Exception e){
            System.out.println(e.getMessage());
        }
    }
}
