package service;

import domain.Friendship;
import domain.Message;
import domain.User;
import repos.Repository;
import repos.db.MessagesDbRepository;
import service.dtos.UserFriendshipDTO;

import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Service {
    private final Repository<User> userRepository;
    private final Repository<Friendship> friendshipRepository;
    private final MessagesDbRepository messagesDbRepository;
    private String currentUserId = null;

    public Service(Repository<User> userRepository, Repository<Friendship> friendshipRepository, MessagesDbRepository messagesDbRepository) {
        this.userRepository = userRepository;
        this.friendshipRepository = friendshipRepository;
        this.messagesDbRepository = messagesDbRepository;
    }

    public boolean loginUser(String email, String password) {
        for (User user : userRepository.getAll()) {
            if (user.getEmail().equals(email)) {
                if (user.getPassword().equals(password)) {
                    currentUserId = user.getId();
                    return true;
                } else {
                    return false;
                }
            }
        }

        return false;
    }

    public boolean createUser(String firstName, String lastName, String email, String password, LocalDate birthdate) {
        try {
            User newUser = new User(null, firstName, lastName, email, password, birthdate);
            userRepository.add(newUser);
            currentUserId = newUser.getId();
        } catch (Error e) {
            System.out.println(e.getMessage());
        }
        return true;
    }

    public void logoutUser() {
        currentUserId = null;
    }

    /**
     * Add a friend to the current user
     *
     * @param friendId String
     */
    public void addFriend(String friendId) {
        User userB = userRepository.getById(friendId);
        User userA = userRepository.getById(currentUserId);
        LocalDate timestamp = LocalDate.now();
        if (userB == null || userA == null) {
            throw new Error("User not found");
        }
        friendshipRepository.add(new Friendship(null, currentUserId, friendId, timestamp, Friendship.STATUS.PENDING));
    }

    /**
     * Remove a friend by id from the current user
     *
     * @param friendshipId
     */
    public void removeFriend(String friendshipId) {
        Friendship friendship = friendshipRepository.getById(friendshipId);
        if (friendship == null) {
            throw new Error("Friendship not found");
        }
        friendshipRepository.remove(friendship);
    }

    public List<Friendship> getUserFriendships() {
        List<Friendship> friendships = new ArrayList<>();
        for (Friendship friendship : friendshipRepository.getAll()) {
            if (friendship.getFriendOneId().equals(currentUserId) || friendship.getFriendTwoId().equals(currentUserId)) {
                friendships.add(friendship);
            }
        }
        return friendships;
    }

    /**
     * Delete the current user account
     */
    public void deleteAccount() {
        if (currentUserId == null) {
            return;
        }
        for (Friendship friendship : getUserFriendships()) {
            friendshipRepository.remove(friendship);
        }
        userRepository.remove(userRepository.getById(currentUserId));
        ;
        currentUserId = null;
    }

    /**
     * Returns the current user id
     *
     * @return String
     */
    public String getCurrentUserId() {
        return currentUserId;
    }

    /**
     * Returns the current user
     *
     * @return User
     */
    public User getCurrentUser() {
        return userRepository.getById(currentUserId);
    }

    /**
     * DFS algorithm
     *
     * @param start   int
     * @param visited boolean[]
     */
    private void DFS(int start, boolean[] visited) {
        List<User> userList = userRepository.getAll();
        visited[start] = true;
        System.out.println(visited[0]);
        for (int x = 0; x < userList.size(); x++) {
            if (!visited[x]) {
                for (Friendship f : friendshipRepository.getAll()) {
                    if (f.getFriendOneId().equals(userList.get(start).getId()) && !visited[userList.indexOf(userRepository.getById(f.getFriendTwoId()))]) {
                        communitySize++;
                        DFS(userList.indexOf(userRepository.getById(f.getFriendTwoId())), visited);
                    }
                    if (f.getFriendTwoId().equals(userList.get(start).getId()) && !visited[userList.indexOf(userRepository.getById(f.getFriendOneId()))]) {
                        communitySize++;
                        DFS(userList.indexOf(userRepository.getById(f.getFriendOneId())), visited);
                    }
                }
            }
        }
    }

    /**
     * Get community numbers
     *
     * @return int
     */
    public int getCommunitiesNumber() {
        List<User> userList = userRepository.getAll();

        int communities = 0;
        boolean[] visited = new boolean[userList.size()];
        for (int i = 0; i < userList.size(); i++) {
            visited[i] = false;
        }

        for (int i = 0; i < userList.size(); i++) {
            if (!visited[i]) {
                DFS(i, visited);
                communities++;
            }
        }

        return communities;
    }

    private int communitySize;

    public int getLargestCommunity() {
        List<User> userList = userRepository.getAll();

        int maxCommunitySize = 0;
        communitySize = 1;
        boolean[] visited = new boolean[userList.size()];
        for (int i = 0; i < userList.size(); i++) {
            visited[i] = false;
        }

        for (int i = 0; i < userList.size(); i++) {
            if (!visited[i]) {
                DFS(i, visited);
                maxCommunitySize = Math.max(maxCommunitySize, communitySize);
                communitySize = 1;
            }
        }

        return maxCommunitySize;
    }

    public void updateUserName(String firstName, String lastName) {
        User user = getCurrentUser();
        user.setFirstName(firstName);
        user.setLastName(lastName);
        userRepository.update(user);
    }

    public List<Friendship> getAllFriendships() {
        return this.friendshipRepository.getAll();
    }

    /**
     * Method that gets all friendships of a given user ID
     *
     * @param UID - ID of the user
     * @return List of UserFriendshipDTO
     */
    public List<UserFriendshipDTO> getUserFriendships(String UID) {
        List<Friendship> all = getAllFriendships();

        return all
                .stream()
                .filter(Friendship::isApproved)
                .filter(friendship -> (friendship.getFriendOneId().equals(UID) || friendship.getFriendTwoId().equals(UID)))
                .map(friendship -> new UserFriendshipDTO(
                                this.userRepository.getById(UID.equals(friendship.getFriendOneId()) ? friendship.getFriendTwoId() : friendship.getFriendOneId()),
                                friendship.getTimestamp()
                        )
                )
                .collect(Collectors.toList());
    }

    /**
     * Method returning the users a user is friends with with the criteria
     * that their friendship started at a given date.
     *
     * @param month between 1 and 12
     * @param UID User's ID
     * @return List of UserFriendshipDTO
     */
    public List<UserFriendshipDTO> getUserFriendshipsByMonth(String UID, int month) {
        return getUserFriendships(UID)
                .stream()
                .filter(userFriendshipDTO -> userFriendshipDTO.getTimestamp().getMonthValue() == month)
                .collect(Collectors.toList());
    }

    /**
     * Method that returns all the messages in chronological order from
     * 2 users.
     *
     * @param UID1 First user's id
     * @param UID2 Second user's id
     * @return List of all the messages in chronological order
     */
    public List<Message> getOrderedMessagesForTwoUsers(String UID1, String UID2) throws SQLException {
        return messagesDbRepository.getAllFor2Users(UID1, UID2);
    }

    public void acceptFriendRequest(String UID){
        for (Friendship friendship : getUserFriendships()){
            if (friendship.getFriendOneId().equals(UID) && friendship.getFriendTwoId().equals(getCurrentUserId()))
                friendship.setStatus(Friendship.STATUS.APPROVED);
                friendshipRepository.update(friendship);
        }
    }


}
