package service.dtos;

import domain.User;

import java.time.LocalDate;

public class UserFriendshipDTO {
    private final String firstName;
    private final String lastName;
    private final LocalDate timestamp;

    /**
     * Data Transfer Object holding a user and a LocalDate from a friendship
     * @param user a User from a friendship
     * @param timestamp LocalDate, the date a friendship started
     */
    public UserFriendshipDTO(User user, LocalDate timestamp) {
        this.firstName = user.getFirstName();
        this.lastName = user.getLastName();
        this.timestamp = timestamp;
    }

    /**
     * Getter method for the timestamp field
     * @return LocalDate timestamp
     */
    public LocalDate getTimestamp() {
        return timestamp;
    }

    /**
     * String formatter method
     * Takes a user's name and a friendship date and returns it by the format
     * firstName | lastName | timeStamp
     * @return String
     */
    public String getNameAndTimestamp(){
        return this.firstName + " | " + this.lastName + " | " + this.timestamp;
    }

    @Override
    public String toString() {
        return getNameAndTimestamp();
    }
}
