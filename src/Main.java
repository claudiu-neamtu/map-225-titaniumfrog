import domain.Friendship;
import domain.User;
import repos.db.FriendshipsDbRepository;
import repos.db.MessagesDbRepository;
import repos.db.UsersDbRepository;
import repos.file.FriendshipRepository;
import repos.Repository;
import repos.file.UsersRepository;
import service.Service;
import ui.UI;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Main {

    public static void main(String[] args) {

        try {
            Connection connection = DriverManager.getConnection("jdbc:postgresql://2.tcp.ngrok.io:18720/social_network", "postgres", "12345678");

            //Repository<User> userRepository = new UsersRepository("users.txt");
            Repository<User> userRepository = new UsersDbRepository(connection);

            //Repository<Friendship> friendshipRepository = new FriendshipRepository("friends.txt");
            Repository<Friendship> friendshipRepository = new FriendshipsDbRepository(connection);

            MessagesDbRepository messagesDbRepository = new MessagesDbRepository(connection);

            Service service = new Service(userRepository, friendshipRepository, messagesDbRepository);

            UI ui = new UI(service);

            ui.start();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}
