package repos.db;

import constants.Constants;
import domain.Friendship;
import domain.Message;
import repos.Repository;

import java.sql.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;

public class MessagesDbRepository implements Repository<Message> {
    private final Connection connection;

    public MessagesDbRepository(Connection connection) {
        this.connection = connection;
    }

    @Override
    public void add(Message entity) {
        String sql = "insert into messages (id, \"from\", \"to\", message, reply, timestamp) values (?, ?, ?, ?, ?, ?)";
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, entity.getId());
            preparedStatement.setString(2, entity.getFrom());
            preparedStatement.setString(3, String.join(",", entity.getTo()));
            preparedStatement.setString(4, entity.getMessage());
            preparedStatement.setString(5, entity.getReply());
            preparedStatement.setTimestamp(6, Timestamp.valueOf(entity.getTimestamp()));
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void remove(Message entity) {
        String sql = "delete from messages where id = ?";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setString(1, entity.getId());
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Message getById(String id) {
        String sql = "select * from messages where id = ? ";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setString(1, id);
            ResultSet resultSet = ps.executeQuery();
            resultSet.next();

            String from = resultSet.getString("from");
            String[] to = resultSet.getString("to").split(",");
            String message = resultSet.getString("message");
            String reply = resultSet.getString("reply");
            LocalDateTime timestamp = resultSet.getTimestamp("timestamp").toLocalDateTime();

            return new Message(id, from, to, message, reply, timestamp);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public List<Message> getAll() {
        ArrayList<Message> messages = new ArrayList<>();
        try {
            PreparedStatement statement = connection.prepareStatement("SELECT * from messages");
            ResultSet resultSet = statement.executeQuery();

            while (resultSet.next()) {
                String id = resultSet.getString("id");
                String from = resultSet.getString("from");
                String[] to = resultSet.getString("to").split(",");
                String message = resultSet.getString("message");
                String reply = resultSet.getString("reply");
                LocalDateTime timestamp = resultSet.getTimestamp("timestamp").toLocalDateTime();

                messages.add(new Message(id, from, to, message, reply, timestamp));
            }
            return messages;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return messages;
    }

    @Override
    public void update(Message entity) {

    }

    public List<Message> getAllFromUser(String userId) {
        ArrayList<Message> messages = new ArrayList<>();
        try {
            PreparedStatement statement = connection.prepareStatement("SELECT * from messages where \"from\" = ? or \"to\" like '%' || ? || '%'");
            statement.setString(1, userId);
            statement.setString(2, userId);
            ResultSet resultSet = statement.executeQuery();

            while (resultSet.next()) {
                String id = resultSet.getString("id");
                String from = resultSet.getString("from");
                String[] to = resultSet.getString("to").split(",");
                String message = resultSet.getString("message");
                String reply = resultSet.getString("reply");
                LocalDateTime timestamp = resultSet.getTimestamp("timestamp").toLocalDateTime();

                messages.add(new Message(id, from, to, message, reply, timestamp));
            }
            return messages;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return messages;
    }

    /**
     * Method returning all messages between 2 users in chronological order
     *
     * @param UID1 User 1 id
     * @param UID2 User 2 id
     * @return List of all messages
     */
    public List<Message> getAllFor2Users(String UID1, String UID2) throws SQLException {
        ArrayList<Message> messages = new ArrayList<>();
        PreparedStatement statement = connection.prepareStatement("SELECT * from messages where (\"from\" = ? OR \"from\" = ?) AND (\"to\" like '%' || ? || '%' OR \"to\" like '%' || ? || '%')");
        statement.setString(1, UID1);
        statement.setString(2, UID2);
        statement.setString(3, UID2);
        statement.setString(4, UID1);
        ResultSet resultSet = statement.executeQuery();

        while (resultSet.next()) {
            String id = resultSet.getString("id");
            String from = resultSet.getString("from");
            String[] to = resultSet.getString("to").split(",");
            String message = resultSet.getString("message");
            String reply = resultSet.getString("reply");
            LocalDateTime timestamp = resultSet.getTimestamp("timestamp").toLocalDateTime();

            messages.add(new Message(id, from, to, message, reply, timestamp));
        }
        return messages;
    }
}
