package repos.file;

import constants.Constants;
import domain.Friendship;

import java.time.LocalDate;
import java.util.Date;

public class FriendshipRepository extends FileRepository<Friendship> {

    public FriendshipRepository(String fileName) {
        super(fileName);
    }

    @Override
    protected Friendship lineToEntity(String line) {
        String []attributes = line.split(",");
        String id = attributes[0];
        String friendOneId = attributes[1];
        String friendTwoId = attributes[2];
        LocalDate timestamp = LocalDate.parse(attributes[3], Constants.STANDARD_DATE_FORMAT);
        String status = attributes[4];
        return new Friendship(id, friendOneId, friendTwoId, timestamp, status);
    }

    @Override
    protected String entityToLine(Friendship friendship) {
        return friendship.getId() + "," + friendship.getFriendOneId() + "," + friendship.getFriendTwoId();
    }

    @Override
    public void update(Friendship entity) {

    }
}
