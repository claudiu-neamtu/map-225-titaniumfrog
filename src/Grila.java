
class PowException extends Exception {
    public PowException(String message) {
        super("PowException: " + message);
    }
}

class NegativeNumberException extends PowException {
    public NegativeNumberException() {
        super("Negative number :(");
    }
}

public class Grila {

    private static int pow(int number, int power) throws NegativeNumberException {
        if (power < 0) {
            throw new NegativeNumberException();
        }
        int result = 1;
        for (int i = 1; i < power; i++) {
            result *= number;
        }
        return result;
    }

    public static void main(String[] args) {
        int a = 10;
        int b = -2;
        int c = 2;
        try {
            System.out.println(1000 / (pow(a, c) - 100) + pow(a, b));
        } catch (NegativeNumberException e) {
            System.out.println(e.getMessage());
        }
    }
}
